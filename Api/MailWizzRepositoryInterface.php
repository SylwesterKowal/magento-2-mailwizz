<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MailWizzRepositoryInterface
{

    /**
     * Save MailWizz
     * @param \Kowal\MailWizz\Api\Data\MailWizzInterface $mailWizz
     * @return \Kowal\MailWizz\Api\Data\MailWizzInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\MailWizz\Api\Data\MailWizzInterface $mailWizz
    );

    /**
     * Retrieve MailWizz
     * @param string $mailwizzId
     * @return \Kowal\MailWizz\Api\Data\MailWizzInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($mailwizzId);

    /**
     * Retrieve MailWizz matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\MailWizz\Api\Data\MailWizzSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete MailWizz
     * @param \Kowal\MailWizz\Api\Data\MailWizzInterface $mailWizz
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\MailWizz\Api\Data\MailWizzInterface $mailWizz
    );

    /**
     * Delete MailWizz by ID
     * @param string $mailwizzId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($mailwizzId);
}

