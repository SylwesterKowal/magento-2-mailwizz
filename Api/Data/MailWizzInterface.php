<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Api\Data;

interface MailWizzInterface
{

    const EMAIL = 'email';
    const EXPORTED = 'exported';
    const MAILWIZZ_ID = 'mailwizz_id';
    const DATE = 'date';

    /**
     * Get mailwizz_id
     * @return string|null
     */
    public function getMailwizzId();

    /**
     * Set mailwizz_id
     * @param string $mailwizzId
     * @return \Kowal\MailWizz\MailWizz\Api\Data\MailWizzInterface
     */
    public function setMailwizzId($mailwizzId);

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     * @param string $email
     * @return \Kowal\MailWizz\MailWizz\Api\Data\MailWizzInterface
     */
    public function setEmail($email);

    /**
     * Get exported
     * @return string|null
     */
    public function getExported();

    /**
     * Set exported
     * @param string $exported
     * @return \Kowal\MailWizz\MailWizz\Api\Data\MailWizzInterface
     */
    public function setExported($exported);

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Kowal\MailWizz\MailWizz\Api\Data\MailWizzInterface
     */
    public function setDate($date);
}

