<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Api\Data;

interface MailWizzSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get MailWizz list.
     * @return \Kowal\MailWizz\Api\Data\MailWizzInterface[]
     */
    public function getItems();

    /**
     * Set email list.
     * @param \Kowal\MailWizz\Api\Data\MailWizzInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

