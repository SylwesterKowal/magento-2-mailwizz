<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Export extends Command
{

    const NAME_ARGUMENT = "name";
    const STORE_ID = "store";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\MailWizz\Controller\Adminhtml\Export\Customers   $customers,
        \Kowal\MailWizz\Controller\Adminhtml\Export\Guests      $guests,
        \Kowal\MailWizz\Controller\Adminhtml\Export\Subscribers $subscribers,
        \Magento\Framework\App\State                            $state
    )
    {
        $this->customers = $customers;
        $this->guests = $guests;
        $this->subscribers = $subscribers;
        $this->state = $state;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or  \Magento\Framework\App\Area::AREA_FRONTEND

        $name = $input->getArgument(self::NAME_ARGUMENT);
        $store = $input->getArgument(self::STORE_ID);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Lista " . $name);

        switch ($name) {
            case 'customers':
                $this->customers->renderBlock = false;
                $this->customers->execute($store);
                break;
            case 'guests':
                $this->customers->renderBlock = false;
                $this->guests->execute($store);
                break;
            case 'subscribers':
                $this->customers->renderBlock = false;
                $this->subscribers->execute($store);
                break;
        }


    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_mailwizz:export");
        $this->setDescription("Export Subscribers, Customers and Guests to MailWizz");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "List: customers, guests, subscribers"),
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "Store ID"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

