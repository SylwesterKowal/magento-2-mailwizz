<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class MailWizzClient extends AbstractHelper
{
    const SECTIONS = 'mailwizz';   // module name
    const GROUPS = 'settings';        // setup general
    const GROUPS_RETURN = 'return';        // setup general

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isEnabled($store_id = 0)
    {
        return $this->getGeneralCfg('enable', $store_id);
    }

    public function getConfig($cfg = null, $store_id = 0)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );
    }

    public function getGeneralCfg($cfg = null, $store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return false;
    }

    public function getReturnCfg($cfg = null, $store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS . '/' . self::GROUPS_RETURN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if (isset($config[$cfg])) return $config[$cfg];
        return $config;
    }

    public function setup($store_id = 0)
    {
        // brak kluczy
        if( !$this->getGeneralCfg('public_key', $store_id) ) return false;
        if( !$this->getGeneralCfg('private_key', $store_id) ) return false;

        $config = new \MailWizzApi_Config(array(
            'apiUrl' => $this->getGeneralCfg('api_url', $store_id),
            'publicKey' => $this->getGeneralCfg('public_key', $store_id),
            'privateKey' => $this->getGeneralCfg('private_key', $store_id),

            // components
            'components' => array(
                'cache' => array(
                    'class' => 'MailWizzApi_Cache_File',
                    'filesPath' => dirname(__FILE__) . '/../MailWizzApi/Cache/data/cache', // make sure it is writable by webserver
                )
            ),
        ));
        // now inject the configuration and we are ready to make api calls
        \MailWizzApi_Base::setConfig($config);

        // start UTC
        //date_default_timezone_set('UTC');

        return true;
    }
}
