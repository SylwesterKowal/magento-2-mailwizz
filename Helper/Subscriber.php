<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\NoSuchEntityException;

class Subscriber extends MailWizzClient
{

    public function addSubscriber($email, $fname, $lastname, $store_id = 0, $list_type = 'default_list_subscribers')
    {
        try {
            $this->setup($store_id);
            $endpoint = new \MailWizzApi_Endpoint_ListSubscribers();

            $response = $endpoint->createUpdate(
                $this->getGeneralCfg($list_type, $store_id), array(
                    'EMAIL' => $email,
                    'FNAME' => $fname,
                    'LNAME' => $lastname,
                    'details' => [ 'status' => 'confirmed' ]
                )
            );

            return $response;

        } catch (\Exception $exception) {
            throw new NoSuchEntityException(__(
                'Could not add Subscriber: %1',
                $exception->getMessage()
            ));
        }
    }


    public function addBulkSubscriber($subscribers, $list = 'default_list_subscribers', $store_id = 0)
    {
        try {
            $this->setup($store_id);
            $endpoint = new \MailWizzApi_Endpoint_ListSubscribers();

            foreach ($subscribers as $subscriber) {
                $endpoint->createUpdate(
                    $this->getGeneralCfg($list, $store_id),
                    $subscriber
                );
            }

        } catch (\Exception $exception) {
            throw new NoSuchEntityException(__(
                'Could not add Subscriber: %1',
                $exception->getMessage()
            ));
        }
    }

    public function unsubscribeByEmail($emial, $store_id = 0)
    {
        try {
            $this->setup($store_id);
            $endpoint = new \MailWizzApi_Endpoint_ListSubscribers();

            $response = $endpoint->unsubscribeByEmailFromAllLists($emial);
            return $response;

        } catch (\Exception $exception) {
            throw new NoSuchEntityException(__(
                'Could not unsubscriber: %1',
                $exception->getMessage()
            ));
        }

    }
}
