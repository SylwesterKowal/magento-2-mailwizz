<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\NoSuchEntityException;

class Lists extends MailWizzClient
{

    public function getLists()
    {
        try {
            if(!$this->setup()) return [];
            $endpoint = new \MailWizzApi_Endpoint_Lists();

            $response = $endpoint->getLists($pageNumber = 1, $perPage = 100);
            if ($response->body['status'] != "success")
                throw new NoSuchEntityException(__('Could not get Lists'));

            $count = $response->body['data']['count'];
            $total_pages = $response->body['data']['total_pages'];
            $current_page = $response->body['data']['current_page'];
            $records = $response->body['data']['records'];

            if($total_pages > 1){
                for ($i = $current_page; $i <= $total_pages; $i++) {
                    $response_ = $endpoint->getLists($pageNumber = $i, $perPage = 100);
                    if ($response_->body['status'] == "success") {
                        $records = array_merge($records, $response_['data']['records']);
                    }
                }
            }
            return $records;

        } catch (\Exception $exception) {
            throw new NoSuchEntityException(__(
                'Could not get Lists: %1',
                $exception->getMessage()
            ));
        }
    }
}
