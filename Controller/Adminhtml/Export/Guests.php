<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Controller\Adminhtml\Export;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Kowal\MailWizz\Controller\Adminhtml\Export\Csv;

class Guests extends Csv implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public $renderBlock = true;

    /**
     * @param PageFactory $resultPageFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Kowal\MailWizz\Helper\Subscriber $subscriber
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Newsletter\Model\Subscriber $subscriberModel
     */
    public function __construct(
        PageFactory                                                $resultPageFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Kowal\MailWizz\Helper\Subscriber                          $subscriber,
        \Magento\Store\Model\StoreManagerInterface                 $storeManager,
        \Magento\Framework\Message\ManagerInterface                $messageManager,
        \Magento\Newsletter\Model\Subscriber                       $subscriberModel
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->subscriber = $subscriber;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->subscriberModel = $subscriberModel;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute($store = null)
    {
        $store_ids = array_keys($this->storeManager->getStores(true));
        $allCustomersByStroerId = [];
        foreach ($store_ids as $store_id) {
            if (!$this->subscriber->isEnabled($store_id)) {
                continue;
            }
            if (!is_null($store) && !empty($store) && $store != $store_id) {
                continue;
            }


            $orderCollecion = $this->orderCollectionFactory
                ->create()
                ->addFieldToSelect('*')
                ->addAttributeToFilter('customer_is_guest', ['eq' => 1])
                ->addAttributeToFilter('store_id', ['eq' => $store_id]);

            $bulkCustomers = [];
            foreach ($orderCollecion as $order) {
                $checkSubscriber = $this->subscriberModel->loadByEmail($order->getCustomerEmail());

                if ($checkSubscriber->isSubscribed()) {
                    // Guest is subscribed
                } else {
                    $billingAddress = $order->getBillingAddress();
                    $bulkCustomers[$order->getCustomerEmail()] = [
                        'EMAIL' => $order->getCustomerEmail(),
                        'FNAME' => $billingAddress->getFirstname(),
                        'LNAME' => $billingAddress->getLastname(),
                        'details' => ['status' => 'confirmed']
                    ];
                }
            }
            $result = $this->subscriber->addBulkSubscriber($bulkCustomers, 'default_list_guests', $store_id);
            $allCustomersByStroerId[$store_id] = $bulkCustomers;
            file_put_contents("_guests_".$store_id.".csv",$this->array2csv($bulkCustomers));
        }
        $this->messageManager->addSuccessMessage(_('Eksport niezarejestrowanych klientów wykonany!'));
        $resultPage = $this->resultPageFactory->create();
        if ($this->renderBlock) {
            if($block = $resultPage->getLayout()->getBlock('export.guests')) {
                $block->setData('customers', $allCustomersByStroerId);
            }
        }
        return $resultPage;
    }
}

