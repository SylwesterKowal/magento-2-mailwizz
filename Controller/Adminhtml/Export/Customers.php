<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Controller\Adminhtml\Export;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Kowal\MailWizz\Controller\Adminhtml\Export\Csv;

class Customers extends Csv implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public $renderBlock = true;

    /**
     * @param PageFactory $resultPageFactory
     * @param \Kowal\MailWizz\Helper\Subscriber $subscriber
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Newsletter\Model\Subscriber $subscriberModel
     */
    public function __construct(
        PageFactory                                 $resultPageFactory,
        \Kowal\MailWizz\Helper\Subscriber           $subscriber,
        \Magento\Customer\Model\CustomerFactory     $customerFactory,
        \Magento\Store\Model\StoreManagerInterface  $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Newsletter\Model\Subscriber        $subscriberModel
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->subscriber = $subscriber;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->subscriberModel = $subscriberModel;
    }


    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute($store = null)
    {
        $store_ids = array_keys($this->storeManager->getStores(true));
        $allCustomersByStroerId = [];
        foreach ($store_ids as $store_id) {
            if (!$this->subscriber->isEnabled($store_id)) {
                continue;
            }
            if (!is_null($store) && !empty($store) && $store != $store_id) {
                continue;
            }

            $customers = $this->customerFactory->create()->getCollection()
                ->addAttributeToSelect("*")
                ->addAttributeToFilter("store_id", array("eq" => $store_id))
                ->load();
            $bulkCustomers = [];
            foreach ($customers as $customer) {
                $checkSubscriber = $this->subscriberModel->loadByEmail($customer->getEmail());

                if ($checkSubscriber->isSubscribed()) {
                    // Customer is subscribed
                } else {
                    $bulkCustomers[] = [
                        'EMAIL' => $customer->getEmail(),
                        'FNAME' => $customer->getFirstname(),
                        'LNAME' => $customer->getLastname(),
                        'details' => ['status' => 'confirmed']
                    ];
                }
            }
            $result = $this->subscriber->addBulkSubscriber($bulkCustomers, 'default_list_customers',$store_id);
            $allCustomersByStroerId[$store_id] = $bulkCustomers;
            file_put_contents("_customers_".$store_id.".csv",$this->array2csv($bulkCustomers));
        }
        $this->messageManager->addSuccessMessage(_('Eksport kontrahentów wykonany!'));
        $resultPage = $this->resultPageFactory->create();
        if ($this->renderBlock) {
            $block = $resultPage->getLayout()->getBlock('export.customers');
            $block->setData('customers', $allCustomersByStroerId);
        }
        return $resultPage;
    }
}

