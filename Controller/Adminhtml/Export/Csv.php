<?php

namespace Kowal\MailWizz\Controller\Adminhtml\Export;

class Csv
{
    public function array2csv($data, $delimiter = ',', $enclosure = '"', $escape_char = "\\")
    {
        $f = fopen('php://memory', 'r+');
        foreach ($data as $item) {
            $item_ = [$item['EMAIL'],$item['FNAME'],$item['LNAME']];
            fputcsv($f, $item_, $delimiter, $enclosure, $escape_char);
        }
        rewind($f);
        return stream_get_contents($f);
    }
}