<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Controller\Adminhtml\Export;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Newsletter\Model\Subscriber;
use Kowal\MailWizz\Controller\Adminhtml\Export\Csv;

class Subscribers extends Csv implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public $renderBlock = true;

    /**
     * @param PageFactory $resultPageFactory
     * @param \Kowal\MailWizz\Helper\Subscriber $subscriber
     * @param \Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory $subscriberCollectionfactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        PageFactory                                                          $resultPageFactory,
        \Kowal\MailWizz\Helper\Subscriber                                    $subscriber,
        \Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory $subscriberCollectionfactory,
        \Magento\Store\Model\StoreManagerInterface                           $storeManager,
        \Magento\Framework\Message\ManagerInterface                          $messageManager
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->subscriber = $subscriber;
        $this->subscriberCollection = $subscriberCollectionfactory;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute($store = null)
    {
        $store_ids = array_keys($this->storeManager->getStores(true));
        $allCustomersByStroerId = [];
        foreach ($store_ids as $store_id) {
            if (!$this->subscriber->isEnabled($store_id)) {
                continue;
            }
            if (!is_null($store) && !empty($store) && $store != $store_id) {
                continue;
            }
            $subscribers = $this->subscriberCollection->create()
                ->addFieldToFilter('store_id', ['eq' => $store_id]);
            $bulkSubscribers = [];
            foreach ($subscribers as $subscriber) {
                $bulkSubscribers[] = [
                    'EMAIL' => $subscriber->getEmail(),
                    'FNAME' => $subscriber->getFirstname(),
                    'LNAME' => $subscriber->getLastname(),
                    'details' => ['status' => 'confirmed']
                ];
            }

            $result = $this->subscriber->addBulkSubscriber($bulkSubscribers, 'default_list_subscribers', $store_id);
            $allCustomersByStroerId[$store_id] = $bulkSubscribers;
            file_put_contents("_subscribers_".$store_id.".csv",$this->array2csv($bulkSubscribers));
        }
        $this->messageManager->addSuccessMessage(_('Eksport subskrybentów wykonany!'));
        $resultPage = $this->resultPageFactory->create();
        if ($this->renderBlock) {
            $block = $resultPage->getLayout()->getBlock('export.subscribers');
            $block->setData('customers', $allCustomersByStroerId);
        }
        return $resultPage;
    }
}

