<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Model;

use Kowal\MailWizz\Api\Data\MailWizzInterface;
use Kowal\MailWizz\Api\Data\MailWizzInterfaceFactory;
use Kowal\MailWizz\Api\Data\MailWizzSearchResultsInterfaceFactory;
use Kowal\MailWizz\Api\MailWizzRepositoryInterface;
use Kowal\MailWizz\Model\ResourceModel\MailWizz as ResourceMailWizz;
use Kowal\MailWizz\Model\ResourceModel\MailWizz\CollectionFactory as MailWizzCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class MailWizzRepository implements MailWizzRepositoryInterface
{

    /**
     * @var ResourceMailWizz
     */
    protected $resource;

    /**
     * @var MailWizzCollectionFactory
     */
    protected $mailWizzCollectionFactory;

    /**
     * @var MailWizzInterfaceFactory
     */
    protected $mailWizzFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var MailWizz
     */
    protected $searchResultsFactory;


    /**
     * @param ResourceMailWizz $resource
     * @param MailWizzInterfaceFactory $mailWizzFactory
     * @param MailWizzCollectionFactory $mailWizzCollectionFactory
     * @param MailWizzSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceMailWizz $resource,
        MailWizzInterfaceFactory $mailWizzFactory,
        MailWizzCollectionFactory $mailWizzCollectionFactory,
        MailWizzSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->mailWizzFactory = $mailWizzFactory;
        $this->mailWizzCollectionFactory = $mailWizzCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(MailWizzInterface $mailWizz)
    {
        try {
            $this->resource->save($mailWizz);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the mailWizz: %1',
                $exception->getMessage()
            ));
        }
        return $mailWizz;
    }

    /**
     * @inheritDoc
     */
    public function get($mailWizzId)
    {
        $mailWizz = $this->mailWizzFactory->create();
        $this->resource->load($mailWizz, $mailWizzId);
        if (!$mailWizz->getId()) {
            throw new NoSuchEntityException(__('MailWizz with id "%1" does not exist.', $mailWizzId));
        }
        return $mailWizz;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->mailWizzCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(MailWizzInterface $mailWizz)
    {
        try {
            $mailWizzModel = $this->mailWizzFactory->create();
            $this->resource->load($mailWizzModel, $mailWizz->getMailwizzId());
            $this->resource->delete($mailWizzModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the MailWizz: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($mailWizzId)
    {
        return $this->delete($this->get($mailWizzId));
    }
}

