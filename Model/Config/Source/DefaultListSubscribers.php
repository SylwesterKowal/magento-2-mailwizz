<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Model\Config\Source;

class DefaultListSubscribers implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Kowal\MailWizz\Helper\Lists $lists
    )
    {
        $this->lists = $lists;
    }

    public function toOptionArray()
    {
        $mailwizz_list = [];
        $lists = $this->lists->getLists();
        foreach ($lists as $list){
            $record = ['value' => $list['general']['list_uid'],'label' => $list['general']['name'].' / '.$list['general']['description']];
            $mailwizz_list[] = $record;
        }
        return $mailwizz_list; //[['value' => 'list1', 'label' => __('list1')],['value' => 'list2', 'label' => __('list2')]];
    }

    public function toArray()
    {
        $arrayList = [];
        $options = $this->toOptionArray();
        foreach ($options as $option){
            $arrayList[$option['value']] = $option['label'];
        }
        return $arrayList; //['list1' => __('list1'),'list2' => __('list2')];
    }
}

