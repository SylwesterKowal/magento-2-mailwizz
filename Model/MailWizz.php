<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Model;

use Kowal\MailWizz\Api\Data\MailWizzInterface;
use Magento\Framework\Model\AbstractModel;

class MailWizz extends AbstractModel implements MailWizzInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\MailWizz\Model\ResourceModel\MailWizz::class);
    }

    /**
     * @inheritDoc
     */
    public function getMailwizzId()
    {
        return $this->getData(self::MAILWIZZ_ID);
    }

    /**
     * @inheritDoc
     */
    public function setMailwizzId($mailwizzId)
    {
        return $this->setData(self::MAILWIZZ_ID, $mailwizzId);
    }

    /**
     * @inheritDoc
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getExported()
    {
        return $this->getData(self::EXPORTED);
    }

    /**
     * @inheritDoc
     */
    public function setExported($exported)
    {
        return $this->setData(self::EXPORTED, $exported);
    }

    /**
     * @inheritDoc
     */
    public function getDate()
    {
        return $this->getData(self::DATE);
    }

    /**
     * @inheritDoc
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }
}

