# Mage2 Module Kowal MailWizz

    ``kowal/module-mailwizz``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Magento 2 MailWizz Integration

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_MailWizz`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-mailwizz`
 - enable the module by running `php bin/magento module:enable Kowal_MailWizz`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (mailwizz/settings/enable)

 - public_key (mailwizz/settings/public_key)

 - private_key (mailwizz/settings/private_key)

 - default_list_subscribers (mailwizz/settings/default_list_subscribers)

 - default_list_customers (mailwizz/settings/default_list_customers)

 - default_list_guests (mailwizz/settings/default_list_guests)

 - api_url (mailwizz/settings/api_url)


## Specifications

 - Console Command
	- Export

 - Controller
	- adminhtml > kowal_mailwizz/export/subscribers

 - Controller
	- adminhtml > kowal_mailwizz/export/customers

 - Controller
	- adminhtml > kowal_mailwizz/export/guests

 - Model
	- MailWizz

 - Observer
	- customer_register_success > Kowal\MailWizz\Observer\Customer\RegisterSuccess

 - Observer
	- newsletter_subscriber_save_after > Kowal\MailWizz\Observer\Newsletter\SubscriberSaveAfter

 - Observer
	- sales_order_save_after > Kowal\MailWizz\Observer\Sales\OrderSaveAfter


## Attributes



