<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Observer\Customer;

class RegisterSuccess implements \Magento\Framework\Event\ObserverInterface
{

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Kowal\MailWizz\Helper\Subscriber                 $subscriber,
        \Magento\Store\Model\StoreManagerInterface        $storeManager,
        \Magento\Newsletter\Model\Subscriber              $subscriberModel
    )
    {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->subscriber = $subscriber;
        $this->storeManager = $storeManager;
        $this->subscriberModel = $subscriberModel;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        if (!$this->subscriber->isEnabled($this->storeManager->getStore()->getId())) {
            return;
        }
        $customer = $observer->getEvent()->getCustomer();

        $checkSubscriber = $this->subscriberModel->loadByEmail($customer->getEmail());

        if ($checkSubscriber->isSubscribed()) {
            // Customer is subscribed
        } else {
            // dodajemy go do listy
            $response = $this->subscriber->addSubscriber(
                $customer->getEmail(),
                $customer->getFirstname(),
                $customer->getLastname(),
                $this->storeManager->getStore()->getId(),
                'default_list_customers'
            );
        }


    }
}

