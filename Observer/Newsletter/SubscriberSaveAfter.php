<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Observer\Newsletter;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Newsletter\Model\Subscriber;

class SubscriberSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Kowal\MailWizz\Helper\Subscriber          $subscriber,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CustomerRepositoryInterface                $customerRepository
    )
    {
        $this->subscriber = $subscriber;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        if (!$this->subscriber->isEnabled($this->storeManager->getStore()->getId())) {
            return;
        }

        /** @var Subscriber $_subscriber */
        $_subscriber = $observer->getDataObject();



        if ( $_subscriber && $_subscriber->hasDataChanges()) {



            $customer = $this->getCustomer($_subscriber);

            if ($_subscriber->isSubscribed()) {

                $response = $this->subscriber->addSubscriber(
                    $customer ? $customer->getEmail() : $_subscriber->getEmail(),
                    $customer ? $customer->getFirstname() : $_subscriber->getFirstname(),
                    $customer ? $customer->getLastname() : $_subscriber->getLastname(),
                    $this->storeManager->getStore()->getId(),
                    'default_list_subscribers'
                );


            } else {
                $this->subscriber->unsubscribeByEmail(
                    $customer ? $customer->getEmail() : $_subscriber->getEmail()
                );
            }
        }

    }

    /**
     * @param Subscriber $_subscriber
     * @return CustomerInterface|null
     */
    private function getCustomer(Subscriber $_subscriber)
    {
        $customer = null;

        if ($_subscriber->getCustomerId()) {
            try {
                $customer = $this->customerRepository->getById($_subscriber->getCustomerId());
            } catch (NoSuchEntityException $e) {
                // If the customer doesn't exist - return null
            }
        }

        return $customer;
    }
}

