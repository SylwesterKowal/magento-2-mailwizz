<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MailWizz\Observer\Sales;

class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Kowal\MailWizz\Helper\Subscriber                 $subscriber,
        \Magento\Store\Model\StoreManagerInterface        $storeManager,
        \Magento\Newsletter\Model\Subscriber              $subscriberModel
    )
    {
        $this->subscriber = $subscriber;
        $this->storeManager = $storeManager;
        $this->subscriberModel = $subscriberModel;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {

        if (!$this->subscriber->isEnabled($this->storeManager->getStore()->getId())) {
            return;
        }

        $order = $observer->getEvent()->getOrder();
        if($order->getCustomerIsGuest()){
            $checkSubscriber = $this->subscriberModel->loadByEmail($order->getCustomerEmail());
            if ($checkSubscriber->isSubscribed()) {
                // Customer is subscribed
            } else {
                // dodajemy go do listy
                $billingAddress = $order->getBillingAddress();
                $this->subscriber->addSubscriber(
                    $order->getCustomerEmail(),
                    $billingAddress->getFirstname(),
                    $billingAddress->getLastname(),
                    $this->storeManager->getStore()->getId(),
                    'default_list_guests'
                );
            }
        }
    }
}

